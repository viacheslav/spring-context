package ru.tsystems.beanpostprocessors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

//@Component
public class HelloWorldBeanPostProcessor implements BeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("Before bean " + beanName + " is initialized.");

//        if (bean instanceof BookService) {
//            return Proxy.newProxyInstance(
//                    BookServiceInterface.class.getClassLoader(),
//                    new Class[]{BookServiceInterface.class},
//                    (proxy, method, methodArgs) -> {
//                        if (method.getName().equals("orderBook")){
//                            System.out.println("Proxy in action.");
//                        }
//                        return method.invoke(bean, methodArgs);
//                    });
//        }

        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("After bean " + beanName + " is initialized.");
        return bean;
    }
}
