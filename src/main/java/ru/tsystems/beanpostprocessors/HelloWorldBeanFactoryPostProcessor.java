package ru.tsystems.beanpostprocessors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

//@Component
public class HelloWorldBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("Bean factory is called.");

//        Call to getBean() will create a bean right here and skip BeanPostProcessors.
//        Use getBeanDefinition() instead.

//        beanFactory.getBean(BookService.class);
//        beanFactory.getBeanDefinition("bookService").setBeanClassName("ru.tsystems.BookDao");
    }
}
