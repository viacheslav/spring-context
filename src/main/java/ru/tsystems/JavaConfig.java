package ru.tsystems;

import org.springframework.context.annotation.Bean;
import ru.tsystems.beanpostprocessors.HelloWorldBeanFactoryPostProcessor;
import ru.tsystems.beanpostprocessors.HelloWorldBeanPostProcessor;
import ru.tsystems.contextlistener.HelloWorldContextListener;

//@Configuration
public class JavaConfig {

    @Bean
    public BookService bookService(BookDao bookDao) {
        BookService bookService = new BookService(bookDao);
        return bookService;
    }

    @Bean
    public BookDao bookDao() {
        BookDao bookDao = new BookDao();
        return bookDao;
    }

    @Bean
    public HelloWorldBeanPostProcessor helloWorldBPP() {
        return new HelloWorldBeanPostProcessor();
    }

    @Bean
    public HelloWorldBeanFactoryPostProcessor helloWorldBFPP() {
        return new HelloWorldBeanFactoryPostProcessor();
    }

    @Bean
    public HelloWorldContextListener helloWorldContextListener() {
        return new HelloWorldContextListener();
    }

}
