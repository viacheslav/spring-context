package ru.tsystems;

import javax.annotation.PostConstruct;

//@Component
public class BookService implements BookServiceInterface {

    //    @Autowired
    private BookDao bookDao;

    public BookService() {
    }

    public BookService(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void init() {
        System.out.println("Init method of the bean. " + this.getClass().getSimpleName());
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("PostConstruct method of the bean. " + this.getClass().getSimpleName());
    }

    public void orderBook() {
        bookDao.saveOrder();
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }
}
